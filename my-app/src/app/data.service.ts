import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { map } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  dataaa: any
  constructor(private http : HttpClient) {
    // this.http.get('http://localhost:4200/assets/mentor.json').subscribe(data => {return data})
  }

  form: FormGroup = new FormGroup({
    $id: new FormControl(null),
    email: new FormControl(''),
    civilities: new FormControl(''),
    first_name: new FormControl(''),
    last_name: new FormControl(''),
    type_company: new FormControl(),
    company_name: new FormControl(),
    user_type: new FormControl(),
    user_status: new FormControl(),
    position: new FormControl()
  })

  getUserData(){
    return this.http.get('https://jsonplaceholder.typicode.com/users')
  }

  getNewData() {
    return this.http.get('http://localhost:4200/assets/mentor.json')
  }

  initializeFormGroup() {
    this.form.setValue({
      $id: null,
      email: '',
      civilities: '',
      first_name: '',
      last_name: '',
      type_company: '',
      company_name: '',
      user_type: '',
      user_status: '',
      position:''
    });
  }

}
