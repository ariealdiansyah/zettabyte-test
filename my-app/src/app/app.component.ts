import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DataService } from './data.service';
import { FormComponent } from './form/form.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  displayedColumns = ['name', 'company.user_type', 'company.name', 'user_status', 'action']
  dataSource!: MatTableDataSource<any>;

  @ViewChild('paginator') paginator!: MatPaginator
  @ViewChild(MatSort) matSort!: MatSort

  constructor(public service: DataService, public dialog: MatDialog) { }

  ngOnInit() {
    // this.service.getUserData().subscribe((res: any) => {
    //   console.log('response', res);
    //   this.dataSource = new MatTableDataSource(res);
    //   this.dataSource.paginator = this.paginator
    //   this.dataSource.sort = this.matSort
    // })

    this.service.getNewData().subscribe((res: any) => {
      console.log('responses', res);
      this.dataSource = new MatTableDataSource(res);
      this.dataSource.paginator = this.paginator
      this.dataSource.sort = this.matSort
    })
  }

  onCreate() {
    this.service.initializeFormGroup();
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    this.dialog.open(FormComponent,dialogConfig);
  }
  filterData($event: any) {
    this.dataSource.filter = $event.target.value;
  }

  title = 'my-app';
}
