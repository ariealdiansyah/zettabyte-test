import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  constructor(public service: DataService) { }

  company_names = [
    { id: 1, value: 'Company' },
  ];

  type_companys = [
    { id: 1, value: 'MCDONALD' },
  ];

  user_types = [
    { id: 1, value: 'HR' },
    { id: 2, value: 'Mentor' },
  ];

  ngOnInit(): void {
  }

  onClear() {
    this.service.form.reset();
    this.service.initializeFormGroup();
  }

}
