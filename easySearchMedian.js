const findMedian = (arr) => {
  const middle = Math.floor(arr.length / 2)
  let sortArr = arr.sort((a, b) => a - b);
  return arr.length % 2 === 0 ? (sortArr[middle - 1] + sortArr[middle]) / 2 : sortArr[middle]
}

const array1 = [8, 7, 7, 9, 5, 4, 2, 9]
console.log(findMedian(array1));