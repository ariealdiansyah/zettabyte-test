const students = [
  {
    id: "1",
    name: "Sherlock",
    score:90
  },
  {
    id: "2",
    name: "Genta",
    score: 75
  },
  {
    id: "3",
    name: "Ai",
    score: 80
  },
  {
    id: "4",
    name: "Budi",
    score:85
  }
]

const result = (arr) => {
let totalScore = 0;
let lenArray = arr.length
arr.forEach((x) => {
  totalScore = totalScore + x.score
})
let meanScore = totalScore / lenArray
let studentList = []
arr.forEach((x) => {
  if(x.score > meanScore) {
    studentList.push(x)
  } 
})

return studentList
}

console.log(result(students))